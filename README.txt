Overview
--------
The node_replace_body.module allows an administrator to run a search and replace on node bodies of any content type.  It is essentially a simple stripped down version of Scanner (http://drupal.org/project/scanner) for Drupal 7.

Requirements
------------
Drupal 7.x
Devel Module

The Devel module is used to display node search results.

Features
--------
  o Search node bodies for specific regex patterns
  o Replace specific regex patterns in node bodies
  o Test Run your regex pattern with any custom text
